# Node CLI todo

## A complete CLI based interface for a todo app in Node

###### Commands to execute

- *npm i*
- *node app --help* : Provides all the commands necessary to run the todo app from the Terminal
- *node app list* : To list all todos created so far
- *node app add -t {title_of_todo} -d {body_of_todo}* : To add a new todo 
- *node app remove -t {title_of_todo_to_remove}* : To remove existing todo with the given title
- *node app update -t {title_of_todo_to_update} -d {new_body_of_todo}* : To update the existing todo with the given title

![todo](https://user-images.githubusercontent.com/42419232/54867219-12678a80-4dac-11e9-8ec4-3d458e7df080.png)
