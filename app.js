const _=require('lodash');
const yargs=require('yargs');
const chalk=require('chalk');
const notes=require('./notes-cli-app/notes');

// console.log(_.concat(arr,45,'crerc',[[4,7,9]]));
// console.log(_.sortBy([4,1,2,2,5,8,6,6]));
const command=yargs.argv._[0];
const {title,desc}=yargs
.command('list','List all todo items')
.command('add','Add a new todo item',{
    title:{
        describe:'Add the title for the new todo item to be created',
        demand:true,
        alias:'t'
    },
    desc:{
        describe:'Description for the new todo item to be created',
        demand:true,
        alias:'d'
    }
})
.command('remove','Remove a todo item',{
    title:{
        describe:'Title of the todo item to be removed',
        demand:true,
        alias:'t'
    }
})
.command('update','Update a todo',{
    title:{
        describe:'Title of the todo item to be updated',
        demand:true,
        alias:'t'
    },
    desc:{
        describe:'New description to be updated in the todo item',
        demand:true,
        alias:'d'
    }
})
.help()
.argv;

if(command==='add'){
    notes.addNote(title,desc,mssg=>{console.log(chalk.green(mssg))});
}else if(command==='remove'){
    notes.removeNote(title,mssg=>{console.log(chalk.red(mssg))})
}else if(command==='update'){
    notes.updateNote(title,desc,mssg=>{console.log(chalk.yellow(mssg))})
}else if(command==='list'){
    notes.listNotes(mssg=>{console.log(chalk.magenta(mssg))})
}
else{console.log("Invalid input");}