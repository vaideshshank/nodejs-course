Node CLI todo

Content covered(major)

->Lodash for utility functions
->Yargs for building CLI app
->Creating a complete todo app with files stored in a json file
->Debugging
    - using the command line using 'node --inspect app' and using 'n' for next line and 'c' for continueing to      the 'debugger' statement if present in the js file or ending the app
    - using repl to get the variable values at a particular line for outputting them on the screen.
->Advanced Yargs

