const _=require('lodash');
const fs=require('fs');
const cfonts=require('cfonts');
const jsonPath='./notes-cli-app/data.json';

cfonts.say('TODO APP',{
    font:'chrome',
    align:'center',
    colors:['#ff0000','#0000ff'],
    letterSpacing:1,
    lineHeight:0.5,
    space:true
});

addNote=(title,desc,callback)=>{
    fs.readFile(jsonPath,'utf-8',function(err,notes){
        if(err){
            fs.writeFile(jsonPath,JSON.stringify([{title,desc}],null,4),null,function(){
                callback(`Added note : ${title}\nDescription : ${desc}`);
                //console.log(chalk.green(`Added note : ${title}\nDescription : ${desc}`));
            })
            return;
        }
        notes=JSON.parse(notes);
        if(_.find(notes,{title})!=undefined){
            callback(`${title} already exists use some other title\n`);
            //console.log(chalk.green(`${title} already exists use some other title\n`));
            return;
        }
        notes.push({
            title,
            desc
        })
        fs.writeFile(jsonPath,JSON.stringify(notes,null,4),null,function(){
            callback(`Added note : ${title}\nDescription : ${desc}`);
            //console.log(chalk.green(`Added note : ${title}\nDescription : ${desc}`));
        })
    })
    
}

removeNote=(title,callback)=>{
    fs.readFile(jsonPath,'utf-8',function(err,notes){
        if(err){
            callback('data.json file doesn\'t exist');
            //console.log(chalk.red('data.json file doesn\'t exist'));
            return;
        }
        notes=JSON.parse(notes);
        if(notes.length==0){callback("No notes exist"); return;}
        _.remove(notes,function(note){
            return note.title==title;
        });
        
        fs.writeFile(jsonPath,JSON.stringify(notes,null,4),function(){
            callback(`Removed note : ${title}`);
            //console.log(chalk.red(`Removed note : ${title}`));
        })
    })
    
}

updateNote=(title,desc,callback)=>{
    fs.readFile(jsonPath,'utf-8',function(err,notes){
        if(err){
            callback('data.json file doesn\'t exist');
            return;
        }
        notes=JSON.parse(notes);
        if(notes.length==0){callback("No notes exist"); return;}
        _.find(notes,{title}).desc=desc;
        fs.writeFile(jsonPath,JSON.stringify(notes,null,4),null,function(){
            callback(`updated note : ${title}\nDescription : ${desc}`);
        })
    })
    
}

listNotes=(callback)=>{
    fs.readFile(jsonPath,'utf-8',function(err,notes){
        if(err){
            callback('data.json file doesn\'t exist');
            return;
        }
        notes=JSON.parse(notes);
        
        if(notes.length==0){callback("No notes exist"); return;}
        
        if(!notes){console.log(callback("data.json file doesn't exist. Read command won't run."))}
        
        _.each(notes,function(val){
            callback(`\nTitle : ${val.title}\nDescription : ${val.desc}\n`);
        });
    })
}


module.exports={
    addNote,
    removeNote,
    updateNote,
    listNotes
}